# Project Thesis

## Performance analysis of a multi-orbital parquet equation solver applied to the Pariser-Parr-Pople model for benzene

[![CI_LaTeX_Compile](https://gitlab.com/yumasheta/mop_proj_thesis/badges/master/pipeline.svg)](https://gitlab.com/yumasheta/mop_proj_thesis/pipelines)
## PDF Download Link:

- [Link](https://gitlab.com/yumasheta/mop_proj_thesis/-/jobs/artifacts/master/raw/thesis.pdf?job=compile_LaTeX)
