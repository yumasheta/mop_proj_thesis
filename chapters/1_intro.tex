% !TeX root = ../thesis.tex

\chapter{Introduction}\label{ch:intro}

Computational methods for solving the electronic structure of solids are the connecting link between solid-state theory and experiments.
The calculations for some response functions, like the optical conductivity, require the evaluation of two-particle correlation functions and typically need to be performed on large compute clusters.
Particularly challenging are systems with strongly correlated electrons.
Recent efforts~\cite{Pudleiner-thesis,pudleiner-benzene,victory} based on the parquet method with one orbital were successful in producing results for the optical conductivity of the benzene molecule and alike.
The main objective of this thesis is the validation and performance analysis of the state-of-the-art \mop{} program developed by \textcite{julian} that employs the parquet method to solve multi-orbital Hubbard models.


\section{Multi-orbital Hubbard model}

The Hubbard model is commonly used in solid-state theory to describe the dynamics of electrons in periodic crystals.
The Hamiltonian in second quantization
\begin{equation}
	\mathcal{H}=-\sum_{ij}t_{ij}\cdag_i\cee_j - \sum_i \mu\,\cdag_i\cee_i + \frac{1}{4}\sum_{ijkl}U_{ikjl}\cdag_i\cdag_j\cee_l\cee_k,
	\label{eq:multi-orb-H}
\end{equation}
is obtained (see~\cite{julian,Rohringer-thesis} for a full derivation) by starting from the Born-Oppenheimer approximation and using the Wannier basis for a tight-binding representation.
Here, \(t_{ij}\) are the hopping amplitudes simulating the kinematics of the electrons and \(\mu\) is the  chemical potential.
The effective Coulomb interaction is encoded in \(U_{ikjl}\), the fully anti-symmetrized two-body integrals satisfying \(U_{ikjl}=-U_{iljk}=-U_{jkil}\).
The indices introduced in \cref{eq:multi-orb-H} collect spin, orbital and site indices.
Hence, the operator \(\cee_i\) (\(\cdag_i\)) annihilates (creates) an electron on a given site and in a given orbital and spin state.


\section{Pariser-Parr-Pople Hamiltonian for benzene}\label{sec:ppp-H}

The benzene molecule \ce{C6H6} forms a highly symmetric (see~\cite{hoerb-benzene} for all symmetries), hexagonal ring.
At each corner sits one carbon atom that is bound to a hydrogen atom.
The \(1s\)-orbitals of the hydrogen atoms hybridize with the \(2s\), \(2p_x\) and \(2p_y\) orbitals of the carbon atoms to form three \(\sigma\)-bonds.
One is consumed by the hydrogen and the other two lie in opposite directions along the ring and connect neighboring carbon atoms.
Additionally, the \(2p_z\)-orbitals of neighboring carbon atoms hybridize into \(\pi\)-bonds.

For low temperatures, the \(\sigma\)-orbitals are assumed to be completely filled.
However, each of the \(\pi\)-orbitals is only half-filled with one of the six remaining electrons.
The dynamics of this system are governed solely by these \(\pi\)-electrons, whereas the \(\sigma\)-electrons are "frozen" and contribute via Coulomb screening.
The semi-empirical model developed by \textcite{PPP1,PPP2,PPP3} captures these properties with a nearest neighbor hopping parameter \(t\) and interpolated Coulomb interactions.
Following~\cite{benzene-params,ohno,Pudleiner-thesis} the interactions are parametrized as \(V_{i} = U / \sqrt{1 + (ar_i)^2},\ i\in\{0,1,2,3\}\), where \(a = U / 5.67 t\) in units of \r{A}\({}^{-1}\).
The inter-atomic distances \(r_i\) are given as \(r_0 = 0\) (on-site), \(r_1=1.4\)\r{A} (next-neighbor) and the remaining two for second-next and third-next neighbors follow from the geometry of the molecule.
\Cref{fig:benzene} pictographically represents the benzene molecule and all interactions in this model.
The \acrfull{PPP} Hamiltonian reads
\begin{equation}
	\begin{split}
		\mathcal{H} &= -t \sum^6_{i=1} \sum_\sigma \big(\cdag_{i\sigma}\cee_{(i+1)\sigma} + \cdag_{(i+1)\sigma}\cee_{i\sigma}\big) - \mu \sum^6_{i=1} \sum_\sigma \cdag_{i\sigma}\cee_{i\sigma} + \sum^6_{i=1} U \cdag_{i\uparrow}\cee_{i\uparrow}\cdag_{i\downarrow}\cee_{i\downarrow} \\
		&\quad + \frac{1}{2} \sum^6_{i=1} \sum_{\sigma\sigma'} \cdag_{i\sigma}\cee_{i\sigma}\Big[V_1 \big(\cdag_{(i+1)\sigma'}\cee_{(i+1)\sigma'} + \cdag_{(i-1)\sigma'}\cee_{(i-1)\sigma'}\big) \\ &\hphantom{\quad \frac{1}{2}} \hphantom{\sum^6_{i=1} \sum_{\sigma\sigma'} \cdag_{i\sigma}\cee_{i\sigma}\Big[+} \mkern+2mu + V_2\big(\cdag_{(i+2)\sigma'}\cee_{(i+2)\sigma'} + \cdag_{(i-2)\sigma'}\cee_{(i-2)\sigma'}\big) + V_3\cdag_{(i+3)\sigma'}\cee_{(i+3)\sigma'} \Big],
		\label{eq:H-benzene}
	\end{split}
\end{equation}
where the chemical potential is given by \(\mu = \frac{1}{2}U + 2V_1 + 2V_2 + V_3\) for half-filling.
In \cref{eq:H-benzene} the spin indices \(\sigma\) are explicit and \(i\) only counts orbitals for a single molecule (site).
Periodic boundary conditions apply to \(i\), ie. \(i=2 \;\hat{=}\; 8 \;\hat{=} -5\).
The numerical values for the parameters of the \acrshort{PPP} Hamiltonian are listed in \cref{tab:benzene-params} in units of the hopping parameter \(t\).
\bigskip

\begin{table}
	\centering
	\caption[Parametrization of the PPP Hamiltonian]%
	{Parametrization of the \acrshort{PPP} Hamiltonian \pcref{eq:H-benzene} in units of \(t\).}%
	\label{tab:benzene-params}
	\begin{tabular}{c c c c c}
		\(V_0 = U\) & \(V_1\) & \(V_2\) & \(V_3\) & \(\mu\) \\
		\toprule
		3.962       & 2.832   & 2.014   & 1.803   & 13.476
	\end{tabular}
\end{table}

In preparation for \mop{}, the Hamiltonian \cref{eq:H-benzene} needs to be split up and provided as the three terms \(\mathcal{H} = H_0 + H_{\mathrm{chem}} + H_{\mathrm{e-e}}\).
These are the single-particle hopping term
\begin{equation}
	H_0 = \sum^{12}_{i,j=1} H_{0, ij} c^\dagger_i c_j = -t \sum^6_{i=1} \sum_\sigma (c^\dagger_{i\sigma}c_{(i+1)\sigma} + c^\dagger_{(i+1)\sigma}c_{i\sigma}),
	\label{eq:benzene-H-hopping}
\end{equation}
the term containing the chemical potential \(\mu\)
\begin{equation}
	H_{\mathrm{chem}} = - \mu \sum^6_{i=1} \sum_\sigma c^\dagger_{i\sigma}c_{i\sigma},
	\label{eq:benzene-H-mu}
\end{equation}
and the two-particle interaction term
\begin{align}
	\begin{split}
		H_{\mathrm{e-e}} & = \sum^6_{i=1} U \cdag_{i\uparrow}\cee_{i\uparrow}\cdag_{i\downarrow}\cee_{i\downarrow} \\
		&\quad + \frac{1}{2} \sum^6_{i=1} \sum_{\sigma\sigma'} \cdag_{i\sigma}\cee_{i\sigma}\Big[V_1 \big(\cdag_{(i+1)\sigma'}\cee_{(i+1)\sigma'} + \cdag_{(i-1)\sigma'}\cee_{(i-1)\sigma'}\big) \\ &\hphantom{\quad \frac{1}{2}} \hphantom{\sum^6_{i=1} \sum_{\sigma\sigma'} \cdag_{i\sigma}\cee_{i\sigma}\Big[+} \mkern+2mu + V_2\big(\cdag_{(i+2)\sigma'}\cee_{(i+2)\sigma'} + \cdag_{(i-2)\sigma'}\cee_{(i-2)\sigma'}\big) + V_3\cdag_{(i+3)\sigma'}\cee_{(i+3)\sigma'} \Big]
		\label{eq:benzene-H-ee-full}
	\end{split} \\
	 & =\frac{1}{4} \sum_{i,j,k,l = 1}^{12} U_{ikjl} c^\dagger_i c^\dagger_j c_l c_k.
	\label{eq:benzene-H-ee}
\end{align}
The actual input required by \mop{} are the matrices \(U_{ikjl}\) and \(H_{0, ij}\), with combined spin-orbital indices, as well as the scalar value of \(\mu\).
The latter two are easily extracted from \cref{eq:benzene-H-hopping,eq:benzene-H-mu}.
The calculation of \(U_{ikjl}\) is more involved and requires careful balancing of \cref{eq:benzene-H-ee,eq:benzene-H-ee-full} for each of the permutations of the operators.
The author has contributed a Python script to the codebase of \mop{} that is capable of preparing and saving these data (see \cref{sec:workflow} for instructions) and a Jupyter notebook for inspecting and validating the saved data.

\begin{figure}
	\centering
	{\fontsize{18pt}{18pt}\input{img_src/benzene.pdf_tex}}
	\caption[Schematic of the benzene molecule.]%
	{Schematic picture of benzene (\ce{C6H6}) showing the \(\sigma\)- and \(\pi\)-bonds, on-site interaction \(U\), neighbor interaction terms \(V_1\), \(V_2\), \(V_3\) and hopping amplitude \(t\) for the \acrshort{PPP} model.}%
	\label{fig:benzene}
\end{figure}
