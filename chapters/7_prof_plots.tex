% !TeX root = ../thesis.tex

% \addtocontents{toc}{\protect\pagebreak}

% imgs count with sections in appendix
\counterwithin{figure}{section}

\chapter{Profiling plots}\label{appx:prof-plots}

The following collection of plots is sorted in groups that combine the different metrics:
\begin{description}
	\item[Time per iteration] is a measure for the time in seconds per iteration.
	\item[Compute] is a measure for the percentage of time per iteration that is spent in compute across all MPI ranks.
	\item[MPI] is a measure for the percentage of time per iteration that is spent at MPI barriers (implicit and explicit) across all MPI ranks.
	\item[MPI time per iteration] is a measure for the time in seconds per iteration that is spent at MPI barriers averaged over all MPI ranks.
\end{description}

Each group \labelcref{appx-sec:grp-a,appx-sec:grp-b,appx-sec:grp-c,appx-sec:grp-e,appx-sec:grp-d,appx-sec:grp-tpi,appx-sec:grp-mem} presents the data in a different way, varying different parameters while keeping others fixed.
The parameters are:
\begin{description}[font=\ttfamily]
	\item[N\_F] for the number of fermionic Matsubara frequencies (affects data size and workload).
	\item[N\_B] for the number of bosonic Matsubara frequencies (affects workload and number of tasks).
	\item[\normalfont\bf Nodes] for the number of used compute nodes.
	\item[\normalfont\bf Tasks per node] for the number of tasks per compute node.
\end{description}

In general, data points are marked with colored diamonds and connected with lines along which some parameter is kept fixed.
Not every parameter is given for every plot but can be determined from the given values.
This is often the case for the number of tasks per node and the value of \fortraninline{N\_B}.
For the standard, diamond-shaped values \mop\ is configured with one task per \fortraninline{N\_B} such that \fortraninline{N\_B} \(=\) \#nodes \(\times\) tasks per node.

The only exception to this rule are the data points marked with colored \xsym{}-symbols.
They correspond to setups where each task is assigned with two \fortraninline{N\_B} and 2\fortraninline{N\_B} \(=\) \#nodes \(\times\) tasks per node.
To emphasize this difference these data are also annotated with "(B2T)", further indicating that any values given in parentheses are associated with "(B2T)" data.
This only applies in the case they differ from the standard values.


\section{Scaling with \texorpdfstring{\fortraninline{N\_F}}{N\_F} and fixed \texorpdfstring{\fortraninline{N\_B}}{N\_B}}%
\label{appx-sec:grp-a}

\Crefrange{fig:grpa-1}{fig:grpa-4} plot the metrics along \fortraninline{N\_F} as the x-axis.
The different colors correspond to each of the values of \fortraninline{N\_B} with a fixed number of tasks per node for each color.
There are three plots per metric with a different number of nodes each.
Scaling in this group corresponds to increasing the workload while keeping the number of tasks fixed.


\section{Scaling with \texorpdfstring{\fortraninline{N\_F}}{N\_F} and fixed number of nodes}%
\label{appx-sec:grp-b}

\Crefrange{fig:grpb-1}{fig:grpb-4} plot metrics along \fortraninline{N\_F} as the x-axis.
The different colors correspond to each of the values for the number of nodes with a fixed value of \fortraninline{N\_B} for each color.
There are three plots per metric with a different number of tasks per node and \fortraninline{N\_F}/\fortraninline{N\_B} ratio each.
Keeping this ratio fixed requires changing the number of nodes for each data point.
For each metric, two plots have \fortraninline{N\_F}/\fortraninline{N\_B} \(=1\), with 8 and 16 tasks per node, and one plot has \fortraninline{N\_F}/\fortraninline{N\_B} \(=2\) with 8 tasks per node.
The latter ratio is considered the sweet spot for physically most meaningful results.
Scaling in this group corresponds to increasing the workload while also adding new tasks on new nodes in a fixed ratio.


\section{Scaling with \texorpdfstring{\fortraninline{N\_B}}{N\_B} and fixed \texorpdfstring{\fortraninline{N\_F}}{N\_F}}%
\label{appx-sec:grp-c}

\Crefrange{fig:grpc-1}{fig:grpc-4} plot metrics along \fortraninline{N\_B} as the x-axis.
The different colors correspond to each of the values for \fortraninline{N\_F}.
There are two plots per metric with a different number of nodes each.
As the number of nodes is fixed per plot, increasing \fortraninline{N\_B} directly translates to increasing the number of tasks per node.
Scaling in this group corresponds to adding more tasks per node but without distributing existing work.
Instead, each task has the same workload regardless of the total number of tasks.


\section{Scaling with nodes and fixed \texorpdfstring{\fortraninline{N\_F}}{N\_F}}%
\label{appx-sec:grp-e}

\Crefrange{fig:grpe-1}{fig:grpe-4} plot the metrics along the number of nodes as the x-axis.
The different colors correspond to each of the values for \fortraninline{N\_F}.
There are two plots per metric with a different number of tasks per node each.
As the number of tasks per node is fixed per plot, increasing the number of nodes directly translates to increasing \fortraninline{N\_B} by the number of tasks per node.
Scaling in this group corresponds to adding more tasks on new nodes but without distributing existing work.
Instead, each task has the same workload regardless of the total number of tasks.


\section{Work distribution scaling}%
\label{appx-sec:grp-d}

\Cref{fig:grpd} plots the metrics along different splits of tasks per node and number of nodes as the x-axis.
The different colors correspond to each of the values for \fortraninline{N\_F}.
There is only one plot per metric and \fortraninline{N\_B} \(=32\).
Scaling in this group corresponds to distributing the same total workload across more nodes rather than more tasks per node.


\section{Comparison of time per iteration scaling with \texorpdfstring{\fortraninline{N\_B}}{N\_B}}%
\label{appx-sec:grp-tpi}

\Crefrange{fig:tpicomp-1}{fig:tpicomp-2} only contain data for the time per iteration.
The first three plots have \fortraninline{N\_B} as their x-axes.
The different colors compare adding tasks as new nodes (with fixed tasks per node) or more tasks per node while keeping the workload fixed.
Each plot has a different value for \fortraninline{N\_F}.
The fourth plot in this group has pairs of \fortraninline{N\_F} and \fortraninline{N\_B} with a fixed ratio of 2 along the x-axis.
There are two colors which either keep the number of nodes or the tasks per node fixed.
Scaling in this group compares increasing the workload while also adding new tasks between either adding tasks per node or nodes.


\section{Memory scaling and requirement prediction}%
\label{appx-sec:grp-mem}

\Crefrange{fig:mem-1}{fig:mem-2} contain plots for the required memory per MPI rank (\(=\) \fortraninline{N\_B}) along \fortraninline{N\_F} as the x-axis.
The measured values are compared to predictions and an \acrshort{LMS} fitted correction.

\clearpage


% grp a
\setcounter{section}{1}
\setcounter{figure}{0}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_Nodes_1_vs_N_F}
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_Nodes_2_vs_N_F}
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_Nodes_4_vs_N_F}
	\caption[Time per iteration scaling with \fortraninline{N\_F} and fixed nodes and \fortraninline{N\_B}.]%
	{Time per iteration scaling with \fortraninline{N\_F} for 1, 2 and 4 nodes. \fortraninline{N\_B} is fixed by color.}%
	\label{fig:grpa-1}
\end{figure}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{Compute_-_Nodes_1_vs_N_F}
	\includesvg[width=0.49\textwidth]{Compute_-_Nodes_2_vs_N_F}
	\includesvg[width=0.49\textwidth]{Compute_-_Nodes_4_vs_N_F}
	\caption[Compute \% scaling with \fortraninline{N\_F} and fixed nodes and \fortraninline{N\_B}.]%
	{Compute \% scaling with \fortraninline{N\_F} for 1, 2 and 4 nodes. \fortraninline{N\_B} is fixed by color.}
\end{figure}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{MPI_-_Nodes_1_vs_N_F}
	\includesvg[width=0.49\textwidth]{MPI_-_Nodes_2_vs_N_F}
	\includesvg[width=0.49\textwidth]{MPI_-_Nodes_4_vs_N_F}
	\caption[MPI \% scaling with \fortraninline{N\_F} and fixed nodes and \fortraninline{N\_B}.]%
	{MPI \% scaling with \fortraninline{N\_F} for 1, 2 and 4 nodes. \fortraninline{N\_B} is fixed by color.}
\end{figure}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{MPI_Time_per_Iteration_-_Nodes_1_vs_N_F}
	\includesvg[width=0.49\textwidth]{MPI_Time_per_Iteration_-_Nodes_2_vs_N_F}
	\includesvg[width=0.49\textwidth]{MPI_Time_per_Iteration_-_Nodes_4_vs_N_F}
	\caption[Per rank average MPI time scaling with \fortraninline{N\_F} and fixed nodes and \fortraninline{N\_B}.]%
	{Per rank average MPI time scaling with \fortraninline{N\_F} for 1, 2 and 4 nodes. \fortraninline{N\_B} is fixed by color.}%
	\label{fig:grpa-4}
\end{figure}

% grp b
\setcounter{section}{2}
\setcounter{figure}{0}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_Tasks_per_Node_8_(4)_vs_N_F}
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_Tasks_per_Node_8_(4)_vs_N_F_sweet_spot}
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_Tasks_per_Node_16_(8)_vs_N_F}
	\caption[Time per iteration scaling with \fortraninline{N\_F} and fixed tasks per node and nodes.]%
	{Time per iteration scaling with \fortraninline{N\_F} for fixed tasks per node. Number of nodes is fixed by color.}%
	\label{fig:grpb-1}
\end{figure}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{Compute_-_Tasks_per_Node_8_(4)_vs_N_F}
	\includesvg[width=0.49\textwidth]{Compute_-_Tasks_per_Node_8_(4)_vs_N_F_sweet_spot}
	\includesvg[width=0.49\textwidth]{Compute_-_Tasks_per_Node_16_(8)_vs_N_F}
	\caption[Compute \% scaling with \fortraninline{N\_F} and fixed tasks per node and nodes.]%
	{Compute \% scaling with \fortraninline{N\_F} for fixed tasks per node. Number of nodes is fixed by color.}
\end{figure}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{MPI_-_Tasks_per_Node_8_(4)_vs_N_F}
	\includesvg[width=0.49\textwidth]{MPI_-_Tasks_per_Node_8_(4)_vs_N_F_sweet_spot}
	\includesvg[width=0.49\textwidth]{MPI_-_Tasks_per_Node_16_(8)_vs_N_F}
	\caption[MPI \% scaling with \fortraninline{N\_F} and fixed tasks per node and nodes.]%
	{MPI \% scaling with \fortraninline{N\_F} for fixed tasks per node. Number of nodes is fixed by color.}
\end{figure}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{MPI_Time_per_Iteration_-_Tasks_per_Node_8_(4)_vs_N_F}
	\includesvg[width=0.49\textwidth]{MPI_Time_per_Iteration_-_Tasks_per_Node_8_(4)_vs_N_F_sweet_spot}
	\includesvg[width=0.49\textwidth]{MPI_Time_per_Iteration_-_Tasks_per_Node_16_(8)_vs_N_F}
	\caption[Per rank average MPI time scaling with \fortraninline{N\_F} and fixed tasks per node and nodes.]%
	{Per rank average MPI time scaling with \fortraninline{N\_F} for fixed tasks per node. Number of nodes is fixed by color.}%
	\label{fig:grpb-4}
\end{figure}

% grp c
\setcounter{section}{3}
\setcounter{figure}{0}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_Nodes_1_vs_N_B}
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_Nodes_2_vs_N_B}
	\caption[Time per iteration scaling with \fortraninline{N\_B} and fixed nodes and \fortraninline{N\_F}.]%
	{Time per iteration scaling with \fortraninline{N\_B} for 1 and 2 nodes. \fortraninline{N\_F} is fixed by color.}%
	\label{fig:grpc-1}
\end{figure}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{Compute_-_Nodes_1_vs_N_B}
	\includesvg[width=0.49\textwidth]{Compute_-_Nodes_2_vs_N_B}
	\caption[Compute \% scaling with \fortraninline{N\_B} and fixed nodes and \fortraninline{N\_F}.]%
	{Compute \% scaling with \fortraninline{N\_B} for 1 and 2 nodes. \fortraninline{N\_F} is fixed by color.}
\end{figure}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{MPI_-_Nodes_1_vs_N_B}
	\includesvg[width=0.49\textwidth]{MPI_-_Nodes_2_vs_N_B}
	\caption[MPI \% scaling with \fortraninline{N\_B} and fixed nodes and \fortraninline{N\_F}.]%
	{MPI \% scaling with \fortraninline{N\_B} for 1 and 2 nodes. \fortraninline{N\_F} is fixed by color.}
\end{figure}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{MPI_Time_per_Iteration_-_Nodes_1_vs_N_B}
	\includesvg[width=0.49\textwidth]{MPI_Time_per_Iteration_-_Nodes_2_vs_N_B}
	\caption[Per rank average MPI time scaling with \fortraninline{N\_B} and fixed nodes and \fortraninline{N\_F}.]%
	{Per rank average MPI time scaling with \fortraninline{N\_B} for 1 and 2 nodes. \fortraninline{N\_F} is fixed by color.}%
	\label{fig:grpc-4}
\end{figure}

% grp e
\setcounter{section}{4}
\setcounter{figure}{0}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_Tasks_per_Node_8_(4)_vs_Nodes}
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_Tasks_per_Node_16_(8)_vs_Nodes}
	\caption[Time per iteration scaling with num.\ of nodes and fixed tasks per node and \fortraninline{N\_F}.]%
	{Time per iteration scaling with num.\ of nodes and fixed tasks per node. \fortraninline{N\_F} is fixed by color.}%
	\label{fig:grpe-1}
\end{figure}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{Compute_-_Tasks_per_Node_8_(4)_vs_Nodes}
	\includesvg[width=0.49\textwidth]{Compute_-_Tasks_per_Node_16_(8)_vs_Nodes}
	\caption[Compute \% scaling with num.\ of nodes and fixed tasks per node and \fortraninline{N\_F}.]%
	{Compute \% scaling with num.\ of nodes and fixed tasks per node. \fortraninline{N\_F} is fixed by color.}
\end{figure}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{MPI_-_Tasks_per_Node_8_(4)_vs_Nodes}
	\includesvg[width=0.49\textwidth]{MPI_-_Tasks_per_Node_16_(8)_vs_Nodes}
	\caption[MPI \% scaling with num.\ of nodes and fixed tasks per node and \fortraninline{N\_F}.]%
	{MPI \% scaling with num.\ of nodes and fixed tasks per node. \fortraninline{N\_F} is fixed by color.}
\end{figure}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{MPI_Time_per_Iteration_-_Tasks_per_Node_8_(4)_vs_Nodes}
	\includesvg[width=0.49\textwidth]{MPI_Time_per_Iteration-_Tasks_per_Node_16_(8)_vs_Nodes}
	\caption[Per rank average MPI time scaling with nodes and fixed tasks per node and \fortraninline{N\_F}.]%
	{Per rank average MPI time scaling with num.\ of nodes and fixed tasks per node. \fortraninline{N\_F} is fixed by color.\vspace{-\baselineskip}}%
	\label{fig:grpe-4}
\end{figure}

% grp d
\setcounter{section}{5}
\setcounter{figure}{0}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_N_B_32_vs_TpN}
	\includesvg[width=0.49\textwidth]{Compute_-_N_B_32_vs_TpN}
	\includesvg[width=0.49\textwidth]{MPI_-_N_B_32_vs_TpN}
	\includesvg[width=0.49\textwidth]{MPI_Time_per_Iteration_-_N_B_32_vs_TpN}
	\caption[Work distribution scaling for fixed \fortraninline{N\_B} and \fortraninline{N\_F}.]%
	{Work distribution scaling for \fortraninline{N\_B} \(=32\) and fixed \fortraninline{N\_F} per color.}%
	\label{fig:grpd}
\end{figure}

% time per iter comparisons
\setcounter{section}{6}
\setcounter{figure}{0}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_N_F_16_vs_N_B}
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_N_F_32_vs_N_B}
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_N_F_64_vs_N_B}
	\caption[Time per iteration scaling with \fortraninline{N\_B} and fixed \fortraninline{N\_F}, nodes or tasks per node.]%
	{Time per iteration scaling with \fortraninline{N\_B} for \fortraninline{N\_F} \(=16,32,64\). Either the num.\ of nodes or tasks per node are fixed by color.}%
	\label{fig:tpicomp-1}
\end{figure}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{Time_per_Iteration_-_N_F_N_B_=_2}
	\caption[Time per iteration scaling with \fortraninline{N\_F} / \fortraninline{N\_B} \(=2\).]%
	{Time per iteration scaling with \fortraninline{N\_F} and \fortraninline{N\_B} keeping the ratio \fortraninline{N\_F} / \fortraninline{N\_B} \(=2\) for fixed number of nodes or tasks per node.}%
	\label{fig:tpicomp-2}
\end{figure}

% memory scaling
\setcounter{section}{7}
\setcounter{figure}{0}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{Memory_per_MPI_Rank_-_MPI_Implementations}
	\includesvg[width=0.49\textwidth]{Memory_per_MPI_Rank_-_OpenMPI}
	\caption[Memory requirement scaling with \fortraninline{N\_F}.]%
	{Required memory in GiB per MPI rank. Comparison of Intel MPI and OpenMPI (left) and correction of memory prediction calculation with \acrshort{LMS} fit (right).}%
	\label{fig:mem-1}
\end{figure}
\begin{figure}
	\centering
	\includesvg[width=0.49\textwidth]{Memory_[Prediction_-_Profiler]_per_MPI_Rank}
	\caption[Predicted minus measured memory requirement.]%
	{Difference of (corrected) memory prediction and profiler measurements.}%
	\label{fig:mem-2}
\end{figure}
