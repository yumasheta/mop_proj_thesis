% !TeX root = ../thesis.tex

\chapter{Multi-orbital parquet method}\label{ch:mop-method}

The parquet method is an iterative approach in the framework of self-consistent-field techniques.
It is based on a closed diagrammatic expansion of the interaction vertex that is treated extensively in literature, eg.~\cite{Pudleiner-thesis,bickers}.
This chapter is a collection of the formulas derived in~\cite{julian} that constitute the parquet method implemented in \mop{}.

We start from the Heisenberg picture for time evolution and adopt imaginary time \(\tau\)
\begin{equation}
	\cdag_i(\tau_i) = \me^{\tau_i\mathcal{H}}\cdag_i\me^{-\tau_i\mathcal{H}}, \qquad \cee_i(\tau_i) = \me^{\tau_i\mathcal{H}}\cee_i\me^{-\tau_i\mathcal{H}}.
	\label{eq:heisenberg-matsubara-picture}
\end{equation}
From there, the one-particle and two-particle Green's functions take the form
\begin{align}
	G_{i_1 i_2}(\tau_1, \tau_2)                         & = -\langle \timeop\,\cee_{i_1}(\tau_1)\,\cdag_{i_2}(\tau_2)\rangle,
	\label{eq:G2}                                                                                                                                                     \\
	G_{i_1 i_2 i_3 i_4}(\tau_1, \tau_2, \tau_3, \tau_4) & = \langle \timeop\,\cee_{i_1}(\tau_1)\,\cdag_{i_2}(\tau_2)\,\cee_{i_3}(\tau_3)\,\cdag_{i_4}(\tau_4)\rangle,
	\label{eq:G4}
\end{align}
where expectation values are evaluated in the grand canonical ensemble as
\begin{equation}
	\begin{aligned}
		G_{i_1 i_2}(\tau_1, \tau_2) & = -\langle \timeop\,\cee_{i_1}(\tau_1)\,\cdag_{i_2}(\tau_2)\rangle                                                                                                                                  \\
		                            & = \frac{1}{\mathrm{Tr}\left[\me^{-\beta\mathcal{H}}\right]} \mathrm{Tr}\left[\timeop\,\me^{-\beta\mathcal{H}}\cee_{i_1}(\tau_1)\,\cdag_{i_2}(\tau_2)\right]                                         \\
		                            & = \frac{1}{\mathrm{Tr}\left[\me^{-\beta\mathcal{H}}\right]} \mathrm{Tr}\left[\timeop\,\me^{-\mathcal{H}(\beta - \tau_1 + \tau_2)}\cee_{i_1}\,\me^{-\mathcal{H}(\tau_1-\tau_2)}\,\cdag_{i_2}\right],
		\label{eq:ensamble-bracket}
	\end{aligned}
\end{equation}
with the inverse temperature \(\beta=\frac{1}{T}\).
In the last line of \cref{eq:ensamble-bracket} we exploited the cyclic property of the trace.
\(\timeop\) is the fermionic imaginary time-ordering operator
\begin{equation}
	\timeop\,\cee_{i_1}(\tau_1)\,\cdag_{i_2}(\tau_2) = \cee_{i_1}(\tau_1)\,\cdag_{i_2}(\tau_2)\,\theta(\tau_1-\tau_2) - \cdag_{i_2}(\tau_2)\,\cee_{i_1}(\tau_1)\,\theta(\tau_2-\tau_1).
\end{equation}
The first exponential factor in \cref{eq:ensamble-bracket} poses restrictions for the possible values of the imaginary time parameters
\begin{equation}
	\beta + \tau_2 > \tau_1 > \tau_2, \qquad \tau_i \in \left[0,\beta\right],
	\label{eq:tau-limits}
\end{equation}
so that even for unbounded spectra of \(\mathcal{H}\) the exponent is always negative and the Green's function stays finite.
Additionally, the anti-periodicity relations hold
\begin{align}
	G_{i_1 i_2}(\tau_1, \tau_2) = - G_{i_1 i_2}(\tau_1 - \beta, \tau_2), \label{eq:anti-per-1} \\
	G_{i_1 i_2}(\tau_1, \tau_2) = - G_{i_1 i_2}(\tau_1, \tau_2 + \beta). \label{eq:anti-per-2}
\end{align}
Based on \cref{eq:anti-per-1,eq:anti-per-2} the Fourier transform of the Green's function
\begin{align}
	G_{i_1 i_2}(\nu_1, \nu_2)   & = \int^\beta_0\DCh{}\tau_1\int^\beta_0\DCh{}\tau_2 \me^{i (\nu_1 \tau_1 + \nu_2 \tau_2)} G_{i_1 i_2}(\tau_1, \tau_2), \\
	G_{i_1 i_2}(\tau_1, \tau_2) & = \frac{1}{\beta^2} \sum_{\nu_1 \nu_2} \me^{-i (\nu_1 \tau_1 + \nu_2 \tau_2)} G_{i_1 i_2}(\nu_1, \nu_2),
\end{align}
returns discrete values for \(\nu_i = (2i+1)\frac{\pi}{\beta}\) with \(i \in \mathbb{Z}\) that are known as the fermionic Matsubara frequencies. The two-particle Green's function has analogous properties that are discussed in~\cite{julian}.

The two-particle Green's function can be expressed via one-particle Green's functions and the full interaction vertex \(F\)
\begin{equation}
	\begin{multlined}
		G_{i_1 i_2 i_3 i_4}(\tau_1, \tau_2, \tau_3, \tau_4) = G_{i_1 i_2}(\tau_1, \tau_2)\:G_{i_3 i_4}(\tau_3, \tau_4)-G_{i_1 i_4}(\tau_1, \tau_4)\:G_{i_3 i_2}(\tau_3, \tau_2)\\
		-G_{i_1 i_5}(\tau_1, \tau_5)\:G_{i_3 i_7}(\tau_3, \tau_7)\:F_{i_5 i_6 i_7 i_8}(\tau_5, \tau_6, \tau_7, \tau_8)\:G_{i_6 i_2}(\tau_6, \tau_2)\,G_{i_8 i_4}(\tau_8, \tau_4).
		\label{eq:G4viaF}
	\end{multlined}
\end{equation}
Whenever indices appear in pairs on different objects summation over spin-orbitals or Matsubara frequencies and integration over imaginary time is implied.
\(F\) can be decomposed into a two-particle reducible part \(\Phi\) and an irreducible part \(\Gamma\) in three scattering channels.
In each channel, the reducible parts are related to the irreducible parts by the Bethe-Salpeter equations
\begin{subequations}\label[subequations]{eqs:bse-pp}
	\begin{align}
		\Phi^\PH_{i_1 i_2 i_3 i_4}(\tau_1, \tau_2, \tau_3, \tau_4)    & =\Gamma^\PH_{i_1 i_2 i_5 i_6}(\tau_1, \tau_2, \tau_5, \tau_6)\:G_{i_6 i_7}(\tau_6, \tau_7)\:G_{i_8 i_5}(\tau_8, \tau_5)\:F_{i_7 i_8 i_3 i_4}(\tau_7, \tau_8, \tau_3, \tau_4),             \\
		\Phi^\PHbar_{i_1 i_2 i_3 i_4}(\tau_1, \tau_2, \tau_3, \tau_4) & =-\Gamma^\PHbar_{i_1 i_6 i_5 i_4}(\tau_1, \tau_6, \tau_5, \tau_4)\:G_{i_6 i_7}(\tau_6, \tau_7)\:G_{i_8 i_5}(\tau_8, \tau_5)\:F_{i_7 i_2 i_3 i_8}(\tau_7, \tau_2, \tau_3, \tau_8),         \\
		\Phi^\PP_{i_1 i_2 i_3 i_4}(\tau_1, \tau_2, \tau_3, \tau_4)    & =\tfrac{1}{2}\Gamma^\PP_{i_1 i_5 i_3 i_6}(\tau_1, \tau_5, \tau_3, \tau_6)\:G_{i_6 i_7}(\tau_6, \tau_7)\:G_{i_5 i_8}(\tau_5, \tau_8)\:F_{i_7 i_2 i_8 i_4}(\tau_7, \tau_2, \tau_8, \tau_4).
	\end{align}
\end{subequations}
Here, \(\PH\) stands for reducibility in the longitudinal particle-hole channel, \(\PHbar\) for reducibility in the transversal particle-hole channel and \(\PP\) for reducibility in the particle-particle channel.
\Cref{fig:bse-red} is the diagrammatic representation of \cref{eqs:bse-pp}.
Reducibility is indicated via a dashed, red line.
In the \(\PH\) channel, the legs labeled with 1 (outgoing) and 2 (incoming) can be separated from 3 (outgoing) and 4 (incoming).
In the \(\PHbar\) channel, the legs labeled with 1 (outgoing) and 4 (incoming) can be separated from 3 (outgoing) and 2 (incoming).
In the \(\PP\) channel, the legs labeled with 1 (outgoing) and 3 (outgoing) can be separated from 2 (incoming) and 4 (incoming).
\begin{figure}
	\centering
	\subfloat[]{\includesvg{bse_ph}\label{sfig:15}} \\ [0.6cm]
	\subfloat[]{\includesvg{bse_ph_bar}\label{sfig:16}} \\ [0.6cm]
	\subfloat[]{\includesvg{bse_pp}\label{sfig:17}}
	\caption[Bethe-Salpeter equations.]{Bethe-Salpeter equations for \(\PH\) (\subref{sfig:15}), \(\PHbar\) (\subref{sfig:16}) and \(\PP\) (\subref{sfig:17}) channels. Reducibility is indicated with a dashed, red line. Images provided by \textcite{julian}.}%
	\label{fig:bse-red}
\end{figure}

The Bethe-Salpeter \cref{eqs:bse-pp} can be reformulated by using the channel-native index convention for which
\begin{equation}
	\begin{aligned}
		F_{i_1 i_2 i_3 i_4}(\tau_1, \tau_2, \tau_3, \tau_4) & = F^\PH_{(i_1 i_2 | i_3 i_4)}(\tau_1, \tau_2, \tau_3, \tau_4)   \\
		                                                    & =F^\PHbar_{(i_1 i_4 | i_3 i_2)}(\tau_1, \tau_4, \tau_3, \tau_2) \\
		                                                    & =F^\PP_{(i_1 i_3 | i_2 i_4)}(\tau_1, \tau_3, \tau_2, \tau_4),
	\end{aligned}
	% required "squeezing" so that overleaf matches my local latex output
	\vspace*{-0.44\baselineskip}
\end{equation}
and \cref{eqs:bse-pp} become
\begin{equation}
	\begin{multlined}
		\Phi^r_{(i_1 i_2 | i_3 i_4)}(\tau_1, \tau_2, \tau_3, \tau_4)= \\
		\kappa_r \Gamma^r_{(i_1 i_2 | i_5 i_6)}(\tau_1, \tau_2, \tau_5, \tau_6)\:X^r_{0,(i_5 i_6 | i_7 i_8)}(\tau_5, \tau_6, \tau_7, \tau_8)\:F^r_{(i_7 i_8 | i_3 i_4)}(\tau_7, \tau_8, \tau_3, \tau_4).\label{eq:bse-native}
	\end{multlined}
\end{equation}
The channel-dependent bare susceptibilities and \(\kappa\)-factors are
\begin{subequations}\label[subequations]{eqs:bare-X}
	\begin{align}
		X^\PH_{0,(i_1 i_2 | i_3 i_4)}(\tau_1, \tau_2, \tau_3, \tau_4)    & = G_{i_2 i_3}(\tau_2, \tau_3)\,G_{i_4 i_1}(\tau_4, \tau_1),  & \kappa_\PH    & = 1,          \\
		X^\PHbar_{0,(i_1 i_2 | i_3 i_4)}(\tau_1, \tau_2, \tau_3, \tau_4) & = G_{i_2 i_3}(\tau_2, \tau_3)\,G_{i_4 i_1}(\tau_4, \tau_1),  & \kappa_\PHbar & = -1,         \\
		X^\PP_{0,(i_1 i_2 | i_3 i_4)}(\tau_1, \tau_2, \tau_3, \tau_4)    & = -G_{i_2 i_3}(\tau_2, \tau_3)\,G_{i_1 i_4}(\tau_1, \tau_4), & \kappa_\PP    & = -\tfrac 12.
	\end{align}
\end{subequations}
In this convention, the Bethe-Salpeter \cref{eq:bse-native} has the same structure for all channels.

We adopt the following shorthand notation to denote the dependency on Matsubara frequencies.
Due to energy conservation, only three frequencies are independent and related as
\begin{equation}\label{eqs:freq_convention}
	\begin{aligned}[c]
		\left.
		\begin{array}{@{\mkern+14mu}l@{\,}l}
			\nu_1 & = \nu         \\
			\nu_2 & = \nu+\omega  \\
			\nu_3 & = \nu'+\omega \\
			\nu_4 & = \nu'
		\end{array}
		\right \} \, \text{for } \PH{} \text{ and } \PHbar{},
	\end{aligned} \qquad
	\begin{aligned}[c]
		\left.
		\begin{array}{@{\,}l@{\,}l}
			\nu_1 & = \nu         \\
			\nu_2 & = \omega-\nu' \\
			\nu_3 & = \omega-\nu  \\
			\nu_4 & = \nu'
		\end{array}
		\right \} \, \text{for } \PP{},
	\end{aligned}
\end{equation}
via the bosonic Matsubara frequency \(\omega\) that assumes discrete values \(\omega_i = 2n_i\frac{\pi}{\beta}\).
This yields
\begin{subequations}\label{eqs:PhiNu}
	\begin{align}
		\Phi^{\PH, \nu \nu' \omega}_{(i_1 i_2 | i_3 i_4)}    & \coloneqq \Phi^{\PH}_{i_1 i_2 i_3 i_4}(\nu,\nu+\omega,\nu'+\omega,\nu'),    \\
		\Phi^{\PHbar, \nu \nu' \omega}_{(i_1 i_2 | i_3 i_4)} & \coloneqq \Phi^{\PHbar}_{i_1 i_4 i_3 i_2}(\nu,\nu',\nu'+\omega,\nu+\omega), \\
		\Phi^{\PP, \nu \nu' \omega}_{(i_1 i_2 | i_3 i_4)}    & \coloneqq \Phi^{\PP}_{i_1 i_3 i_2 i_4}(\nu,\omega-\nu,\omega-\nu',\nu'),
	\end{align}
\end{subequations}
and analogously for \(F^r\) and \(\Gamma^r\).


\section{Iteration steps}\label{sec:iter-steps}

The iteration algorithm of \mop{} consists of a series of steps that each update an object.
In the following, objects that were updated in any previous step of the current iteration are marked with a prime, compared to their non-primed counterparts from the previous iteration.
Furthermore, due to crossing symmetry between the \(\PH\) and \(\PHbar\) channels
\begin{equation}
	\Phi^{\PHbar,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} = -\Phi^{\PH, \nu(\nu'+\omega)(\nu'-\nu)}_{(i_1 i_2 | i_3 i_4)},
	\label{eq:PhiCrossingphphbar}
\end{equation}
and the calculations only need to be performed for the \(\PH\) and \(\PP\) channels.

\subsubsection*{Step 1: Bethe-Salpeter equations}

\begin{subequations}\label[subequations]{eqs:impBSE}
	\begin{align}
		\Phi'\,{}^{\PH,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} & = \frac{1}{\beta^2}\sum_{\nu_1\nu_2}\kappa_{\PH}\Gamma^{\PH,\nu\nu_1\omega}_{(i_1 i_2 | i_5 i_6)} X^{\PH,\nu_1\nu_2\omega}_{0\,(i_5 i_6 | i_7 i_8)}F^{\PH,\nu_2\nu'\omega}_{(i_7 i_8 | i_3 i_4)} \\
		\Phi'\,{}^{\PP,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} & = \frac{1}{\beta^2}\sum_{\nu_1\nu_2}\kappa_{\PP}\Gamma^{\PP,\nu\nu_1\omega}_{(i_1 i_2 | i_5 i_6)} X^{\PP,\nu_1\nu_2\omega}_{0\,(i_5 i_6 | i_7 i_8)}F^{\PP,\nu_2\nu'\omega}_{(i_7 i_8 | i_3 i_4)}
	\end{align}
\end{subequations}
The Bethe-Salpeter \cref{eqs:impBSE} are used to update the reducible parts \(\Phi\) of the vertex \(F\).
The intermediate object
\begin{equation}
	\tilde{F}^{r,\nu_1\nu'\omega}_{(i_5 i_6 | i_3 i_4)} \coloneqq X^{r,\nu_1\nu_2\omega}_{0\,(i_5 i_6 | i_7 i_8)}F^{r,\nu_2\nu'\omega}_{(i_7 i_8 | i_3 i_4)},
	\label{eq:ftilde}
\end{equation}
is used to calculate the terms of the sum in two steps for each of the two channels \(r \in \{\PH, \PP\}\).
Afterward, over-relaxation is applied and the updated value for \(\Phi'\) is mixed with the old one according to
\begin{equation}
	\Phi'\,^{r} = (1-\alpha)\Phi'\,^{r} + \alpha \Phi^r,
\end{equation}
with the mixing parameter \(\alpha \in [0,1]\).

\subsubsection*{Step 2: Parquet equations}

\begin{subequations}\label[subequations]{eqs:parquet-eq}
	\begin{align}
		F'\,{}^{\PH,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} & = \Lambda^{\PH,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} + \Phi'\,{}^{\PH,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} - \Phi'\,{}^{\PH,\nu(\nu+\omega)(\nu'-\nu)}_{(i_1 i_4 | i_3 i_2)} + \Phi'\,{}^{\PP,\nu\nu'(\omega+\nu+\nu')}_{(i_1 i_3 | i_2 i_4)}  \\
		F'\,{}^{\PP,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} & = \Lambda^{\PP,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} + \Phi'\,{}^{\PP,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} + \Phi'\,{}^{\PH,\nu\nu'(\omega-\nu-\nu')}_{(i_1 i_3 | i_2 i_4)} - \Phi'\,{}^{\PH,\nu(\omega-\nu')(\nu'-\nu)}_{(i_1 i_4 | i_2 i_3)}
	\end{align}
\end{subequations}
The parquet \cref{eqs:parquet-eq} decompose the full interaction vertex \(F\) into a fully irreducible part \(\Lambda\) (the channel denotes the native index convention not reducibility) and parts reducible in the \(\PP\) and \(\PH\) channels.
The \(\Phi'\), that were updated in previous step, are used to update \(F\).

\subsubsection*{Step 3: Channel-wise decomposition}

\begin{equation}
	\Gamma'\,{}^{r,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} = F'\,{}^{r,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} - \Phi'\,{}^{r,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)}\label{eq:gamma-ch}
\end{equation}
The simpler decomposition \cref{eq:gamma-ch} for each channel \(r\) is used to update the channel-wise irreducible parts \(\Gamma\).

\subsubsection*{Step 4: Schwinger-Dyson equation}

\begin{equation}
	\begin{aligned}
		\Sigma'_{i_1 i_2}(\nu) = & - U_{i_1 i_3 i_4 i_2}G_{i_3 i_4}(\tau=0^-)                                                                                                                                                                            \\
		                         & -\frac{1}{2\beta^3}\sum_{\nu'\nu''\omega}U_{i_1 i_3 i_4 i_5}\,G_{i_3 i_8}(\nu+\omega)X^{\PH, \nu'\nu''\omega}_{0\,(i_4 i_5 | i_6 i_7)}\big(F'\,{}^{\PH,\nu''\nu\omega}_{(i_6 i_7 | i_8 i_2)}-U_{i_6 i_7 i_8 i_2}\big) \\
		                         & + \frac{1}{2}\int_0^{\beta}\DCh\tau\,U_{i_1 i_3 i_4 i_5}U_{i_6 i_7 i_8 i_2}G_{i_3 i_8}(\tau)G_{i_4 i_6}(\tau)G_{i_7 i_5}(\beta-\tau)\me^{-\mi\nu\tau}
		\label{eq:impSDE}
	\end{aligned}
\end{equation}
The Schwinger-Dyson \cref{eq:impSDE} is used to calculate the self-energy \(\Sigma\) from the Green's function, the \(U\)-matrix of the lattice potential and the interaction vertex \(F\).
Single-particle objects that only depend on one imaginary time argument are to be interpreted as
\begin{equation}
	G_{i_1 i_2}(\tau) \coloneqq G_{i_1 i_2}(\tau=\tau_1 - \tau_2, 0) = G_{i_1 i_2}(\tau_1, \tau_2),
	\label{eq:gtau-1arg}
\end{equation}
where the last equality follows from the time-translation symmetry of the Green's function.
One-particle objects only depend on one frequency due to energy conservation.
The Fourier transformation from \(G(\nu) \rightarrow G(\tau)\) implicitly used in \cref{eq:impSDE} is modified to better match the asymptotic behavior for large frequencies
\begin{equation}
	G_{i_1 i_2}(\tau) = \frac{1}{\beta} \sum_\nu (G_{i_1 i_2}(\nu) - \frac{1}{i\nu})\me^{-i\nu\tau} - \frac{1}{2}.
	\label{eq:gtau-ft}
\end{equation}
Similarly, the full Schwinger-Dyson \cref{eq:impSDE} is also modified by subtracting the asymptotic part of the interaction vertex \(F\) and adding a simpler term that is quadratic in the \(U\)-matrix instead.

\subsubsection*{Step 5: Dyson equations}

\begin{subequations}\label[subequations]{eqs:impDE}
	\begin{align}
		G_{0, i_1 i_2}(\nu) & = \left[i\nu + \mu - H_0\right]^{-1}                                  \\
		G'_{i_1 i_2}(\nu)   & = \left[G_{0, i_1 i_2}(\nu)^{-1} - \Sigma'_{i_1 i_2}(\nu)\right]^{-1}
	\end{align}
\end{subequations}
For the last step, the Dyson \cref{eqs:impDE} are used to update the Green's function.
Here, \(G_0\) is the non-interacting Green's function that is calculated from the chemical potential and non-interacting part \(H_0\) of the full Hamiltonian.

\subsubsection*{Initialization}

In order to start the iteration process, initial values for all objects are needed:
\begin{align}
	\Sigma_{i_1 i_2}(\nu)                              & = 0, \label{eq:init1}                                                                                                      \\
	\Phi\,{}^{r,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)}   & = 0,                                                                                                                       \\
	\Gamma\,{}^{r,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} & = \Lambda^{r,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)},                                                                         \\
	F\,{}^{r,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)}      & = \Gamma\,{}^{r,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} = \Lambda^{r,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} \label{eq:init-fin}
\end{align}
The only object not set by \crefrange{eq:init1}{eq:init-fin} is the fully irreducible part \(\Lambda\).
It is initialized using the parquet approximation
\begin{equation}
	\begin{split}
		\Lambda^{\PH,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} \approx \big(U_{i_1 i_2 i_3 i_4}\big)_{\nu\nu'\omega}\ , \\
		\Lambda^{\PP,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)} \approx \big(U_{i_1 i_3 i_2 i_4}\big)_{\nu\nu'\omega}\ ,
	\end{split}
	\label{eq:parquet-approx}
\end{equation}
with the instantaneous, bare interaction \(\big(U_{i_1 i_2 i_3 i_4}\big)_{\nu\nu'\omega}\) obtained from
\begin{equation}
	U_{i_1 i_2 i_3 i_4}(\tau_1, \tau_2, \tau_3, \tau_4) = \sum_{ijkl}U_{ikjl}\delta(\tau_1-\tau_2)\delta(\tau_2-\tau_3)\delta(\tau_3-\tau_4)\delta_{i_1,i}\delta_{i_2,k}\delta_{i_3,j}\delta_{i_4,l}.\label{eq:U}
\end{equation}
