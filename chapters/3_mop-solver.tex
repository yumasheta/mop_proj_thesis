% !TeX root = ../thesis.tex

% putting \mop{} or \fortraninline in chapter name protected by texorpdfstring
% DOES NOT WORK ONLY FOR THIS CHAPTER FOR SOME REASON
\chapter{The {\ttfamily multi-orbital-parquet} solver}\label{ch:solver}

\begin{table}[p]
	\centering
	\caption[External dependencies of \mop{}.]{
		External dependency libraries of \mop{}.
		The solver is verified to run with the listed library versions.
	}\label{tab:mop-deps}
	\begin{tabular}{c c}
		\toprule
		                 & Version                                           \\
		\midrule
		FORTRAN compiler & \emph{gfortran} 10.2 and \emph{ifort} 19.1.3      \\
		CMake            & 3.15.1                                            \\
		LAPACK           & via \emph{Intel-MKL} 2020.1                       \\
		HDF5             & 1.10.5                                            \\
		MPI              & \emph{Open MPI} 4.1.1 and \emph{Intel MPI} 2019.7 \\
		Python           & 3.8                                               \\
		\bottomrule
	\end{tabular}
\end{table}
\begin{figure}[p]
	\centering
	\includegraphics[width=\textwidth,clip,trim=.5in .5in .5in .5in]{modules.pdf}%
	\caption{Dependency tree for the modules of \mop{}.}%
	\label{fig:module-dep}
\end{figure}

The \mop{} solver is a FORTRAN%
\footnote{The source code is written in free form and uses features from FORTRAN90 onwards.}
program developed by \textcite{julian} that implements the parquet method introduced in \cref{ch:mop-method}.
For ease of use, a CMake build system is configured that compiles the source code and links external libraries.
\Cref{tab:mop-deps} summarizes the tools and libraries required to build and run \mop{} and lists version numbers that are verified to work.
An up-to-date FORTRAN compiler is mandatory, but both the gcc and Intel toolsets are possible.
The CMake version has to be 3.15.1 or higher to ensure compatibility with the configuration files.
The solver makes use of a few LAPACK routines and a high-performance implementation like the \acrfull{Intel-MKL} is recommended.
Because the data output of the solver is exclusively stored in HDF5 format, this library is required as well.
The core design of \mop{} is built around massive parallelization with \acrshort{MPI} for use on large compute clusters.
Additional helper scripts, that are written in Python, are included in the codebase.
These are not a requirement for running the solver but help with preparation and data inspection.
A requirements file with additional packages for use with \emph{pip} in a Python virtual environment is provided.

The codebase of \mop{} is organized in modules.
Their dependency tree is depicted in \cref{fig:module-dep} in hierarchical order from top to bottom.
The \fortraninline{main\_program} module is the entry point and sets the flow of the program starting from the setup (\fortraninline{setup\_routines} and \fortraninline{mpi\_routines}) and then launching the \fortraninline{parquet\_iteration}.
It is also responsible for clean-up after the iteration completes.
Whether or not the iteration converged is determined with checks from the \fortraninline{convergence\_routines}.
The \fortraninline{parquet\_iteration} module depends on separate modules for the steps discussed in \cref{ch:mop-method}.
The \acrfull{bse} (\ref{eqs:impBSE}), \acrfull{pe} (\ref{eqs:parquet-eq}) and \acrfull{sde} (\ref{eq:impSDE}) are implemented in their respective modules and the Dyson \cref{eqs:impDE} are part of the \fortraninline{one\_particle\_functions}.
Calculations shared between the different steps are implemented in \fortraninline{two\_particle\_functions}, which handle vertex operations, and in \fortraninline{one\_particle\_functions}, which handle operations on the one-particle Green's function.
The \fortraninline{fourier\_routines} and \fortraninline{f\_gamma\_functions} were contributed by Anna Kauch and are based on~\cite{victory}.
The interface to the highly-dimensional objects for storing the vertices and Green's function is implemented in the \fortraninline{auxiliary\_functions} and \fortraninline{index\_type} modules.
At the bottom of the hierarchy is the \fortraninline{parameters} module that provides all compile-time constants.
The routines in the \fortraninline{print\_routines} module are used to log the configured setup and ongoing progress of the iterations to the standard output stream.


% table is part of next section, but this way it's on the floats only page
\begin{table}
	\centering
	\caption[Multidimensional objects' implemented dimensionality.]{
		Dimensionality of the implemented objects in comparison to their natural dimensions.
		Spin-orbital indices are written as \(i_1,i_2,i_3,i_4\), frequency indices as \(\nu, \nu'\) for fermionic and \(\omega\) for bosonic frequencies.
		\fortraninline{N\_SO} denotes the number of spin-orbital states, \fortraninline{N\_F} and \fortraninline{N\_B} the number of fermionic and bosonic frequencies respectively.
	}\label{tab:obj-dims}
	\begin{tabular}{l c c}
		\toprule
		Object                                                 & Natural dimensions                                                                                                                                                                   & Implemented dimensions                                                                                                                                            \\
		\midrule
		\(\mathcal{H}_{0,i_1 i_2}\)                            & {\footnotesize (\fortraninline{N\_SO} | \fortraninline{N\_SO})                                     }                                                                                 & {\footnotesize (\fortraninline{N\_SO} | \fortraninline{N\_SO})                                  }                                                                 \\
		\(U_{i_1 i_2 i_3 i_4}\)                                & {\footnotesize (\fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_SO})                     }                                                 & {\footnotesize (\fortraninline{N\_SO}\({}^2\) | \fortraninline{N\_SO}\({}^2\))                  }                                                                 \\
		\(G_{i_1 i_2}^\nu\)                                    & {\footnotesize (\fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_F})                              }                                                                 & {\footnotesize (\fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_F})                           }                                                 \\
		\(\Sigma_{i_1 i_2}^\nu\)                               & {\footnotesize (\fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_F})                              }                                                                 & {\footnotesize (\fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_F})                           }                                                 \\
		\(F_{(i_1 i_2 | i_3 i_4)}^{r, \nu \nu' \omega}\)       & {\footnotesize (\fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_F} | \fortraninline{N\_F} | \fortraninline{N\_B})} & {\footnotesize (\fortraninline{N\_SO}\({}^2\) \fortraninline{N\_F} | \fortraninline{N\_SO}\({}^2\) \fortraninline{N\_F }| \fortraninline{N\_B}) }\hspace*{-8.3pt} \\
		\(\Gamma_{(i_1 i_2 | i_3 i_4)}^{r, \nu \nu' \omega}\)  & {\footnotesize (\fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_F} | \fortraninline{N\_F} | \fortraninline{N\_B})} & {\footnotesize (\fortraninline{N\_SO}\({}^2\) \fortraninline{N\_F} | \fortraninline{N\_SO}\({}^2\) \fortraninline{N\_F }| \fortraninline{N\_B}) }\hspace*{-8.3pt} \\
		\(\Phi_{(i_1 i_2 | i_3 i_4)}^{r, \nu \nu' \omega}\)    & {\footnotesize (\fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_F} | \fortraninline{N\_F} | \fortraninline{N\_B})} & {\footnotesize (\fortraninline{N\_SO}\({}^2\) \fortraninline{N\_F} | \fortraninline{N\_SO}\({}^2\) \fortraninline{N\_F }| \fortraninline{N\_B}) }\hspace*{-8.3pt} \\
		\(\Lambda_{(i_1 i_2 | i_3 i_4)}^{r, \nu \nu' \omega}\) & {\footnotesize (\fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_SO} | \fortraninline{N\_F} | \fortraninline{N\_F} | \fortraninline{N\_B})} & {\footnotesize (\fortraninline{N\_SO}\({}^2\) \fortraninline{N\_F} | \fortraninline{N\_SO}\({}^2\) \fortraninline{N\_F }| \fortraninline{N\_B}) }\hspace*{-8.3pt} \\
		\bottomrule
	\end{tabular}
\end{table}

\pagebreak
\section{Implementation}\label{sec:implem}

In the previous chapters, we introduced objects with varying dimensions and indices.
Their implementation in \mop{} does not always follow their natural dimensionality.
While one-particle objects, such as \(G^\nu_{i_1 i_2}, \Sigma^\nu_{i_1 i_2}\) and the Hamiltonian \(\mathrm{H}_{0,i_1 i_2}\), are saved as multidimensional arrays that mirror the structure of these objects, two-particle objects, such as the interaction matrix \(U_{ikjl}\) and the vertices \(F_{(i_1 i_2 | i_3 i_4)}^{r, \nu \nu' \omega}\) with all of their reducible and irreducible parts in all channels, are stored as three-dimensional arrays instead of seven-dimensional arrays.
This reduction in dimensions is achieved by combining spin-orbital and frequency indices in the same fashion as spin and orbital indices are combined.
Given the orbital index \(o\) and the spin index \(\sigma\), the combined spin-orbital index \(i\) is calculated as \(i = \sigma + 2(o-1)\) using the FORTRAN index convention%
\footnote{In the FORTRAN index convention array indices start from 1, as opposed to the C index convention where indices start from 0.}.
For \(U_{ikjl}\) the two spin-orbital indices \(i\) and \(k\) (or \(j\) and \(l\)) are combined to \(i = \sigma_i + 2(o_i-1) + 12\left[(\sigma_k -1) + 2(o_k -1)\right]\) as there are 12 spin-orbital states for benzene.
The vertices combine two spin-orbital indices with one fermionic frequency index each.
For the frequency indices, we now use the same symbols as previously for the frequencies themselves.
This gives \(i = \sigma_1 + 2(o_1-1) + 12\left[(\sigma_2 -1) + 2(o_2 -1)\right] + 12^2(\nu -1)\) (similar for \(i_3\) and \(i_4\) with \(\nu'\)) and leaves the third dimension exclusively to the bosonic frequency \(\omega\).
\Cref{tab:obj-dims} compares the different objects in terms of their dimensionalities.
\fortraninline{N\_SO} denotes the number of spin-orbital states, \fortraninline{N\_F} and \fortraninline{N\_B} the number of fermionic and bosonic frequencies respectively.
Due to the complex conjugation and time-reversal symmetries discussed in~\cite{julian}
\begin{equation}
	\big(\Phi^{r,\nu\nu'\omega}_{(i_1 i_2 | i_3 i_4)}\big)^{*} = \Phi^{r,(-\nu)(-\nu')(-\omega)}_{(i_1 i_2 | i_3 i_4)},
\end{equation}
and it turns out that it is only necessary to save half of the bosonic frequencies' values in addition to the 0 frequency.
Therefore, the values for the frequencies are related to the frequency indices via \(\nu=(2\nu+1)\frac{\pi}{\beta}\) with \(\nu=-\frac{1}{2}\text{\fortraninline{N\_F}},\ldots,\frac{1}{2}\text{\fortraninline{N\_F}}-1\) spreading the fermionic frequencies equally around but excluding 0 and \(\omega=2\omega\frac{\pi}{\beta}\) with \(\omega=0,\ldots,\text{\fortraninline{N\_B}}-1\) for bosonic frequencies.

The benefit of combining the indices in this way is that it makes it possible to implement summation over spin-orbital and frequency indices as matrix-matrix multiplications and to employ readily available, high-performance routines from the LAPACK library to do so.
In particular, the \acrshort{bse} (\ref{eqs:impBSE}) require two multiplications.
First, the bare susceptibilities \(X_0\) are calculated on-demand with \cref{eqs:bare-X} for fixed \(\nu_1\) and \(\nu_2\).
To get the intermediate \(\tilde{F}\), the necessary matrix-matrix product is calculated according to \cref{eq:ftilde} using the \fortraninline{ZGEMM} routine.
The actual shape of \(X_0\) leads to diagonalization in the \(\nu_1, \nu_2\) subspace \parencite[see][]{julian} so that the multiplied matrices only have (\fortraninline{N\_SO}\({}^2\) | \fortraninline{N\_SO}\({}^2\)) dimensions.
Then, \(\tilde{F}\) is multiplied with \(\Gamma\) using the \fortraninline{ZGEMM} routine again.
This time, the involved matrices have the shape (\fortraninline{N\_SO}\({}^2\) \fortraninline{N\_F} | \fortraninline{N\_SO}\({}^2\) \fortraninline{N\_F}).
Finally, these steps are repeated for every bosonic \(\omega\) independently because the \acrshort{bse} do not mix in \(\omega\).

The situation is similar for the \acrshort{sde} (\ref{eq:impSDE}).
On the second line, the bare susceptibility in the \(\PH\) channel is calculated the same way as before and the product of \(X_0(F-U)\) is again obtained via \fortraninline{ZGEMM} on (\fortraninline{N\_SO}\({}^2\) | \fortraninline{N\_SO}\({}^2\)) matrices.
A second (\fortraninline{N\_SO}\({}^2\) | \fortraninline{N\_SO}\({}^2\)) \fortraninline{ZGEMM} call appears in the multiplication with the first \(U\) and the Green's functions \(G\) is multiplied in by hand.
This calculation is repeated for every \(\nu\) and the result is then summed over all
\(\omega\).
The first and last line of \cref{eq:impSDE} are implemented by hand and require the Fourier transformation of the Green's function to imaginary time \(\tau\) via \cref{eq:gtau-ft}.

Apart from \fortraninline{ZGEMM}, the two LAPACK routines \fortraninline{ZGETRF} and \fortraninline{ZGETRI} are also used in \mop{}.
The Dyson \cref{eqs:impDE} invert a (\fortraninline{N\_SO} | \fortraninline{N\_SO}) matrix for each frequency \(\nu\) in the calculation of the one-particle Green's function.


\subsection{Parallelization with MPI}\label{subsec:mpi-par}

The main challenge for the implementation and execution of the parquet method is the large memory requirement.
As is visible from \cref{tab:obj-dims}, the amount of memory consumed scales roughly as \(\mathcal{O}\)(\fortraninline{N\_SO}\({}^4\times\)\fortraninline{N\_F}\({}^2\times\)\fortraninline{N\_B}).
For example, with \fortraninline{N\_B}=32, \fortraninline{N\_F}=64 and 12 spin-orbital states already \(\sim400\)GB are needed%
\footnote{The value is taken from actual runs of \mop{}.}.
It is clear, that in order to match or surpass the parameter space of existing codes (like~\cite{victory} with \(>\)300 \fortraninline{N\_F}) distributing the data set and workload across multiple nodes in a high-performance compute cluster is necessary.
Therefore, \mop{} implements massive parallelization based on the \acrfull{MPI} 4.0 standard in its most up-to-date version at the time of writing.
Still, the legacy FORTRAN \acrshort{MPI} bindings described in section 19.1.3 of the \acrshort{MPI} 4.0 standard~\cite{mpi-std} are used instead of the new \fortraninline{mpi\_f08} module.

Examining the equations of the parquet method discussed in \cref{sec:iter-steps} leads to a straightforward parallelization scheme.
With the exception of the parquet \cref{eqs:parquet-eq}, all fundamental steps of the iteration process are diagonal in \(\omega\).
This means that the calculation of these steps can be performed for each \(\omega\)'s values independently and therefore on separate processes or \acrshort{MPI} ranks.
Furthermore, because the vertices are stored with an extra array dimension dedicated to \(\omega\), the data structure is ideally suited for parallelization along the \(\omega\)-dimension.

The details of the \acrshort{MPI} routines used are as follows.
Each rank has its own copy of the one-particle objects (\(G, \Sigma\)) and the two-particle vertices (\(F, \Phi, \Gamma, \Lambda\)) are distributed across the ranks along the \(\omega\)-dimension.
In step 1 the \acrshort{bse} (\ref{eqs:impBSE}) are solved by each rank independently for the range of \(\omega\) values assigned to it.
A major complication has to be sorted out for the \acrshort{pe} (\ref{eqs:parquet-eq}) in step 2 because of the way different \(\omega\)'s values are mixed together.
The implemented solution in \mop{} is to loop over all ranks and \fortraninline{MPI\_BCAST} the \(\Phi\) for the \(\omega\) values of the rank matching the loop index to all other ranks.
Then the sum is calculated on all the ranks that require the broadcasted values and saved to their \(F\) vertices.
Step 3 is again computed on each rank independently.
For the \acrshort{sde} (\ref{eq:impSDE}) in step 4, first, a single rank computes the "correction terms" that correspond to the first and last lines of \cref{eq:impSDE}.
Then, all ranks calculate the summands in the second line that correspond to their \(\omega\) values.
The final sum over \(\omega\) is then performed as a call to \fortraninline{MPI\_ALLREDUCE} so that all ranks have the updated values for the self-energy \(\Sigma\).
In this way, the correction terms are naturally added once to the summed up values of \(\Sigma\).
One parquet iteration is completed with step 5 after all ranks update their Green's function according to the Dyson \cref{eqs:impDE} using the new \(\Sigma\).

The key points of the implementation of \mop{} with \acrshort{MPI} are the repeated calls to \fortraninline{MPI\_BCAST} and the single call to \fortraninline{MPI\_ALLREDUCE} for each parquet iteration.
The one-particle objects are redundantly saved and calculated on all ranks and the two-particle vertices are distributed across the ranks for different \(\omega\) values.
This distribution of data leads to parallelization of the parquet iteration along the \(\omega\)-dimension of the data.


\section{Workflow}\label{sec:workflow}

In this section we go over step by step how to set up and execute the \mop{} solver.
We start with preparing the necessary input that consists of the two HDF5 data files \fortraninline{h0\_input.hdf5} and \fortraninline{u\_matrix\_input.hdf5}.
These two files contain the single-particle hopping term \(H_0\) and two-particle interaction term \(H_{\mathrm{e-e}}\) of the Hamiltonian that describes the system to be solved.
The multidimensional objects need to be stored using the index convention introduced in \cref{sec:implem} in FORTRAN column-major order.

% parameters.ini file contents
\begin{lstlisting}[
	language=bash,
	float=t,
	basicstyle=\ttfamily\footnotesize,
	breaklines=true,
	frame=tb,
	numbers=left,
	numberstyle=\tiny,
	captionpos=b,
	label=lst:parameters-ini,
	caption={[Example \fortraninline{parameters.ini} file.]Example content for \fortraninline{parameters.ini} used to configure \mop{}.},
	belowcaptionskip=\baselineskip%,
]
! to be stored in /input
N_ORBITAL 1
N_F 20
N_B 20
BETA 1.0
MU 0.25
MAX_ITE 50
ABS_TOL_SIGMA_MEAN 1.0e-8
ABS_TOL_SIGMA_MAX 1.0e-7
REL_TOL_SIGMA 1.0e-6
ABS_TOL_F 1.0e-7
REL_TOL_F 1.0e-6
ALL_SELF_ENERGIES_BOOL .FALSE.
ALL_G_TAU_BOOL .FALSE.
READ_IN_LAMBDA_ONLY_BOOL .FALSE.
READ_IN_LAMBDA_PHI_BOOL .FALSE.
\end{lstlisting}

% benzene_input.py help prompt
%\begin{noindent}
\begin{lstlisting}[
	float=!ht,
	basicstyle=\ttfamily\footnotesize,
	breaklines=true,
	postbreak=\mbox{\textcolor{red}{\(\hookrightarrow\)}\space},
	frame=tb,
	numbers=left,
	numberstyle=\tiny,
	captionpos=b,
	label=lst:input-py,
	caption=Help prompt of the \pythoninline{benzene_input.py} script.,
	belowcaptionskip=\baselineskip%,
]
$ python input/benzene_input.py --help
usage: benzene_input.py [-h] [-o path/to/input] [-t t] [--fferm N_F]         [--fbos N_B] [--beta BETA] [--max-ite MAX_ITE]

Configure parameters and generate input matrices for benzene. Optional arguments will only be written to file if they are set. Unspecified optional arguments are taken from the existing file.

options:
    -h, --help            Show this help message and exit.
    -o path/to/input, --output path/to/input
                          Path to folder for saving generated files. Defaults to dir of this script.
    -t t, --hopping t     Hopping parameter 't' in the Hamiltonian.
    --fferm N_F           Number of fermionic frequencies to sample.
    --fbos N_B            Number of bosonic frequencies.
    --beta BETA           Inverse temperature.
    --max-ite MAX_ITE     Maximum number of iterations.
\end{lstlisting}
%\end{noindent}

By design, \mop{} is highly configurable.
It reads the configuration from a file called \fortraninline{parameters.ini} during the setup phase.
On each line a parameter is listed and separated by a space from the value it assumes.
An example file is given in \cref{lst:parameters-ini}.
The lines 2 to 6 contain parameters that further control the physics and set the system size.
The lines 7 to 12 determine the break condition for the parquet iteration.
If either the maximum number of iterations \fortraninline{MAX\_ITE} is reached or convergence is achieved according to all of the given tolerances \mop{} stops.
The boolean flags on lines 13 and 14 allow to save the values of the imaginary time-dependent Green's function \(G(\tau)\) and self-energy \(\Sigma(\nu)\) for all iteration steps instead of only for the last two steps, which is the default.
When the flag on line 15 is activated, \mop{} expects values for the \(\Lambda\) parts of the vertices \(F\) in the \(\PH\) and \(\PP\) channels that will be used instead of the parquet approximation \cref{eq:parquet-approx}.
Similarly, the flag on the last line of \cref{lst:parameters-ini} configures \mop{} to read in the values for \(\Lambda\) and \(\Phi\) in both scattering channels.
All of the required input files need to be placed in a folder called \fortraninline{input/} that has to be placed directly in the working directory when executing \mop{}.

In order to deal with the complex preparation of input requirements, helper scripts are provided in the codebase.
For the \acrshort{PPP} Hamiltonian discussed in \cref{sec:ppp-H}, there is the Python script \fortraninline{benzene\_input.py} available.
The output of its \fortraninline{--help} prompt is included in \cref{lst:input-py}.
This script writes the \fortraninline{h0\_input.hdf5} and \fortraninline{u\_matrix\_input.hdf5} files in units of and scaled to the hopping set by \fortraninline{-t} to the output directory set by \fortraninline{-o}.
A matching parameters file will also be saved to this directory.
Though, it is required that there already exists a template file in that location or the save location of the script because only the parameters that are set on the command line are updated and this may result in an incomplete configuration.

Additionally, the codebase contains the Jupyter notebook \fortraninline{input\_validation.ipynb}.
It explains in detail how the input matrices are extracted from the \acrshort{PPP} Hamiltonian and provides means to inspect and check the input HDF5 files generated by the \fortraninline{benzene\_input.py} script.

Compilation requires all the dependencies listed in \cref{tab:mop-deps} to be available.
First, CMake needs to configure the compiler setup and then compiles \mop{} to the \fortraninline{bin/} folder.
In a terminal change to the root directory of the codebase and run CMake via:
\begin{lstlisting}[language=Bash,basicstyle=\ttfamily\footnotesize]
	$ cd multi-orbital-parquet
	$ cmake -B build -DCMAKE_BUILD_TYPE=Release
	$ cmake --build build
\end{lstlisting}
The CMake configuration will be written to the \fortraninline{build/} directory.
Three build types are available.
The \emph{Release} target sets compiler flags for the most possible optimization of the code on the target platform.
The \emph{Debug} target sets all of the available debugging flags.
There is also the \emph{Profile} target that uses flags specifically required by the profiler used in \cref{ch:prof}.

Before \mop{} can be started, the output directory must be prepared.
There has to exist a folder called \fortraninline{output/} in the working directory when launching the solver.
\acrshort{MPI} applications are typically started with
\begin{lstlisting}[language=Bash,basicstyle=\ttfamily\footnotesize]
	$ mpirun -np <n_tasks> ./bin/multi-orbital-parquet.out
\end{lstlisting}
where the number of \acrshort{MPI} processes is given by \fortraninline{<n\_tasks>}.
Note, that the value for \fortraninline{N\_B} set in the parameters file has to be integer divisible by the number of \acrshort{MPI} processes.
This is a limitation of the parallelization scheme discussed in \cref{subsec:mpi-par}.

During execution \mop{} logs information to the standard output stream.
This output of a typical run is given in \cref{lst:stdout}.
Before the iteration starts, all of the used parameters are dumped.
A subset of these are also written to the \fortraninline{general\_data.hdf5} output file.
Afterward follows a prediction for the total required memory and how it is split up across the \acrshort{MPI} ranks.
The progress of the parquet iteration will be displayed in a progress bar below that.
Information about the average time per iteration and estimated, remaining runtime is also visible.
If \mop{} terminated cleanly, regardless if the iteration converged or not, the output log will end with error values for the self-energy \(\Sigma\) and the two channels of the vertex \(F\).
The error values are calculated from the differences of the results from the last two iteration steps.

% example stdout
% \begin{noindent}
\begin{lstlisting}[
	float=tbph,
	basicstyle=\ttfamily\footnotesize,
	breaklines=true,
	postbreak=\mbox{\textcolor{red}{\(\hookrightarrow\)}\space},
	frame=tb,
	numbers=left,
	numberstyle=\tiny,
	captionpos=b,
	label=lst:stdout,
	caption=Example of the log \mop{} writes to \emph{stdout}.,
	belowcaptionskip=\baselineskip%,
]
------------------------
------ Parameters ------
------------------------
Obligatory parameters:
  N_ORBITAL = 6
  N_F = 80
  N_B = 40
  MAX_ITE = 250
  BETA = 0.300E+01
  MU = 0.135E+02
Advanced parameters (default values, when not set):
  ALL_SELF_ENERGIES_BOOL = T
  ALL_G_TAU_BOOL = T
  READ_IN_LAMBDA_PHI_BOOL = F
  READ_IN_LAMBDA_ONLY_BOOL = F
  ABS_TOL_SIGMA_MEAN = 0.100E-07
  ABS_TOL_SIGMA_MAX = 0.100E-06
  REL_TOL_SIGMA = 0.100E-05
  ABS_TOL_F = 0.100E-06
  REL_TOL_F = 0.100E-05
  ALPHA_MIX = 0.400E+00
------------------------

Projected memory usage (OpenMPI 4.1): 
813.849 GB (total)    20.346 GB (per MPI Rank) [ +   0.001 GB (for Rank=0)]

Start iteration...

Iteration step: 250 / 250    15.30 Min/Iter 0:00:00                            [########################################] 100.0%

Iteration did not converge within 250 steps.

ABS_ERR_SIGMA_MEAN = 0.122E-12
ABS_ERR_SIGMA_MAX = 0.535E-11
REL_ERR_SIGMA = 0.429E-10
ABS_ERR_F_PH_MAX = 0.217E+02
REL_ERR_F_PH_MAX = 0.200E+01
ABS_ERR_F_PP_MAX = 0.214E+02
REL_ERR_F_PP_MAX = 0.200E+01
\end{lstlisting}
% \end{noindent}
